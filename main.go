package main

import (
	_ "embed"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/canvas"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/driver/desktop"
	"fyne.io/fyne/v2/widget"
)

func main() {
	appl := app.NewWithID("lucia.scarlet.i.have.no.idea.what.im.doing")
	win := appl.NewWindow("Cleaner")
	win.SetMaster()
	win.CenterOnScreen()
	win.RequestFocus()
	win.Resize(fyne.Size{Width: 800, Height: 400})
	//cleanIconResource := theme.NewPrimaryThemedResource(resourceIconPng)

	headerImage := canvas.NewImageFromResource(resourceIconPng)
	headerImage.SetMinSize(fyne.Size{Height: 64, Width: 64})

	headerTitle := widget.NewRichTextFromMarkdown("# The Keyboard Cleaner")
	headerSubtitle := widget.NewLabel("Press the button below so you can start cleaning your Mac keyboard without the risk to open any app. The application will go full screen.")
	headerSubtitle.Wrapping = fyne.TextWrapWord

	btnClean := widget.NewButtonWithIcon("Start!", resourceIconPng, func() {
		win.SetFullScreen(true)

		headerSubtitle.Text = "Press CTRL+SHIFT+P to exit."
		headerSubtitle.Refresh()

		ctrlShiftP := &desktop.CustomShortcut{KeyName: fyne.KeyP, Modifier: fyne.KeyModifierControl | fyne.KeyModifierShift}
		win.Canvas().AddShortcut(ctrlShiftP, func(shortcut fyne.Shortcut) {
			appl.Quit()
		})
	})
	btnClean.Importance = widget.HighImportance

	headerTitleAndImg := container.NewHBox(headerTitle, headerImage)

	centerAll := container.NewCenter(container.NewVBox(headerTitleAndImg, widget.NewSeparator(), headerSubtitle, btnClean))
	win.SetContent(centerAll)

	win.Show()
	appl.Run()
}
